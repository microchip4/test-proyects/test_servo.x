/*
 * File:   main_Servo.c
 * Author: Brandy
 * Reference: https://electrosome.com/servo-motor-pic-mplab-xc8/
 * Created on October 21, 2016, 2:18 AM
 */

#include <xc.h>
#include <stdlib.h>
#include "Alteri.h"
#include "PCD8544.h"

int i;
long Servo=140;
char ServoC[4];
int Grados=0;
char GradosC[4];

void main(void) {
    ADCON0=0x00; //all digital
    ADCON1=0x00; //all digital
    ADCON1bits.PCFG=0x0F;
    //OSCCONbits.IRCF=0b110; //Fosc=8MHz
    TRISA=0xFF;
    TRISD=0x00;
    TRISB=0x00; //PORTB output
    EYRLCD_Init();
    EYRLCD_Clear();
    delay_ms(10);
    while(1){
        EYRLCD_WriteString("Delay de Servo: ",0,0);
        EYRLCD_WriteString("Grados: ",0,2);
        itoa(ServoC,Servo,10);
        EYRLCD_WriteString(ServoC,0,1);
        itoa(GradosC,Grados,10);
        EYRLCD_WriteString(GradosC,0,3);
        Servo=((long)(2.4*Grados+140));
        for (i = 0; i < 20; i++) {
            LATDbits.LATD4=1;
            delay_us(Servo);
            LATDbits.LATD4=0;
            delay_us(4400-Servo);
        }
        if (Servo>580)
            Servo=580;
        if (Grados>180)
            Grados=180;
        if(PORTAbits.RA3!=1)
            Grados+=10;
        if(PORTAbits.RA1!=1)
            Grados-=10;
    }
    return;
}
